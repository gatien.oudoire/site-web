const navSlide = () => {
    const menu = document.querySelector('.menu-barres');
    const nav = document.querySelector('.nav-liens');
    const navLinks = document.querySelectorAll('.nav-liens li')

    menu.addEventListener('click', () => {
        nav.classList.toggle('nav-actif');
    });

    navLinks.forEach((link, index) => {
        link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7}s`
    }); 
}

navSlide();